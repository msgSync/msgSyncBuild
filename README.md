# MsgSync Build

The intention of this build is to create a single docker image to host the MsgSync application.

I wanted an application to share messages between multiple devices.  
A single container would host the app while file system storage would serve as persistence.

The front end is a Progressive Web Application created with React.

The backend uses FeathersJS.