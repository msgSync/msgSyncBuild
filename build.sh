#!/bin/bash

# https://medium.com/@shakyShane/lets-talk-about-docker-artifacts-27454560384f

git clone https://gitlab.com/msgSync/msgSyncServer.git

cd ./msgSyncServer

git pull

cd ..

git clone https://gitlab.com/msgSync/msgSyncPWA.git

cd ./msgSyncPWA

git pull

cd ..

docker build . -t fubz/msgsync