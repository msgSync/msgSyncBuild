FROM node:14 as builder

# Who(m) to blame if nothing works
MAINTAINER fubz.dev@gmail.com

# Set production build
ARG NODE_ENV=production

# Create a working directory
RUN mkdir -p /usr/src/app

# Switch to working directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
# http://bitjudo.com/blog/2014/03/13/building-efficient-dockerfiles-node-dot-js/
COPY msgSyncPWA/package*.json ./

# Install dependencies (if any) in package.json
RUN npm install

# Copy contents of local folder to `WORKDIR`
# You can pick individual files based on your need
COPY msgSyncPWA/ ./

RUN ls -l .

# Build the application
RUN npm run build

######
# Setup Server

FROM node:14-alpine

# Who(m) to blame if nothing works
MAINTAINER fubz.dev@gmail.com

# Set production build
ARG NODE_ENV=production

# Create a working directory
RUN mkdir -p /usr/src/app

# Switch to working directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
# http://bitjudo.com/blog/2014/03/13/building-efficient-dockerfiles-node-dot-js/
COPY msgSyncServer/package*.json ./

# Install dependencies (if any) in package.json
RUN npm install

# Copy contents of local folder to `WORKDIR`
# You can pick individual files based on your need
COPY msgSyncServer/ ./

RUN ls -l .

COPY --from=builder /usr/src/app/build /usr/src/app/public

# Expose port from container so host can access 3000
EXPOSE 3030

# Start the Node.js app on load
CMD [ "npm", "start" ]